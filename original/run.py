# -*- codintg:utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
import pip

data = pd.read_csv('internet-traffic-data-in-bits-fr.csv', sep=',', parse_dates=[0], 
                    header=0,  names=['time','data'])

# print data.loc[0]
ticks = []
xs = []
for i,d in enumerate(data['time']):
    if i==0 or ticks[-1] != d.day:
        ticks.append(d.day)
        xs.append(i)  

fig = plt.figure()
plt.xticks(xs[::3], ticks[::3])
plt.plot(data.index, data['data'])

series = zip(data.index,data['data'])
pips = [series[pip_index] for pip_index in pip.get_pips(series, numbers=5000)]

xdata, ydata = zip(*pips)
plt.scatter(xdata, ydata, marker='o', color='red', s=80, alpha=.6)
plt.show()    
