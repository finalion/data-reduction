# coding: utf-8
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

# fig = plt.plot(test_x, test_y, marker='o')
# # fig = plt.plot(test_x, test_y, marker='o', c=test_y,
# #                   s=100, cmap = plt.get_cmap('Spectral'))
# for point in test_series:
#     plt.annotate("(%.1f, %.1f)"%point, xy=point, xytext=np.array(point)+np.array([-1,0.1]))


# In[5]:

def get_pip(input_series, index_first, index_last, dtype='e'):
    index = 0
    d = -1
    series = input_series[index_first:index_last + 1]
    for i, point in enumerate(series[1:-1]):
        dt = distance(point, series[0], series[-1], dtype)
        # print index_first+1+i, point, dt
        index, d = (i + 1, dt) if dt > d else (index, d)
    return index + index_first


def distance(pc, pl, pr, dtype):
    # make sure coordinates of the input points are float type
    d = 0
    if dtype == "e":  # euclidean distance
        dl = np.sqrt((pc[0] - pl[0])**2 + (pc[1] - pl[1])**2)
        dr = np.sqrt((pc[0] - pr[0])**2 + (pc[1] - pr[1])**2)
        d = dl + dr
    elif dtype == "v":        # vertical distance
        if pl[1] != pr[1]:
            line_slope = (pr[1] - pl[1]) / (pr[0] - pl[0])
            line_intercept = (pr[0] * pl[1] - pr[1] * pl[0]) / (pr[0] - pl[0])
            cross_x = pc[0]
            cross_y = line_slope * cross_x + line_intercept
        else:
            cross_y = pr[1]
        d = np.abs(pc[1] - cross_y)
    elif dtype == "p":    # perpendicular distance
        cl = np.sqrt((pc[0] - pl[0])**2 + (pc[1] - pl[1])**2)
        cr = np.sqrt((pc[0] - pr[0])**2 + (pc[1] - pr[1])**2)
        lr = np.sqrt((pl[0] - pr[0])**2 + (pl[1] - pr[1])**2)
        t = (cl + cr + lr) / 2
        d = 2 * np.sqrt(t * (t - cl) * (t - cr) * (t - lr)) / lr
    return d

# In[ ]:


def get_pips(series, **kwargs):
    # parse the parameter. if 'numbers' specified,
    # then yield 'numbers' points, otherwise, output all the points by
    # importance order.
    numbers = kwargs.get('numbers', len(series))
    if numbers > len(series):
        numbers = len(series)
    # print numbers, '>>>>>>>>>>>>>>>>>>>>>>'
    input_series = series
    index_first, index_last = 0, len(input_series) - 1
    processing = [index_first, index_last]
    # pips = [index_first, index_last]
    yield index_first
    yield index_last
    i = 0
    while len(processing) != numbers:
        index_first, index_last = processing[i], processing[i + 1]
        if index_last == index_first + 1:  # two adjacent points
            i += 1
        # [i0, i2], in this serie, only have one point
        elif index_last == index_first + 2:
            index_pip = index_first + 1
            processing.insert(i + 1, index_pip)
            # pips.append(index_pip)
            yield index_pip
            i += 2
        else:
            index_pip = get_pip(input_series, index_first, index_last, 'v')
            processing.insert(i + 1, index_pip)
            # pips.append(index_pip)
            yield index_pip
            i += 2

        # reach the end of the processing list, restart to find unprocessed
        # data point
        if i == len(processing) - 1:
            i = 0
    # return pips


def update_plot(gene, series, pips):
    new_pip_index = gene
    pip = series[new_pip_index]
    pips.append(pip)
    # x_data, y_data = zip(*points)
    # print x_data, y_data
    # line.set_data(x_data, y_data)50
    collection.set_offsets(pips)
    return collection


def init():
    # generate the data sets
    global test_series
    test_x, test_y = np.arange(numbers), np.random.rand(numbers) * 10
    test_series = zip(test_x, test_y)
    print test_y
    lines.set_data(test_x, test_y)
    return lines,

if __name__ == '__main__':
    #-------------------------------------------------
    pip_measure = 'e'  # e: euclidean, v: vertical, p: perpendicular
    data_length = 1000
    pip_numbers = 1000
    #-------------------------------------------------
    test_x, test_y = np.arange(data_length), np.random.rand(data_length) * 10
    test_series = zip(test_x, test_y)
    pips = []

    fig = plt.figure()
    axes = plt.axes(xlim=(-1, data_length + 1), ylim=(-1, 11))
    lines, = axes.plot(test_x, test_y, marker='.', color='green')
    collection = axes.scatter([], [], marker='o', color='red', s=300, alpha=.6)

    ani = animation.FuncAnimation(fig, update_plot, frames=get_pips(test_series, numbers=pip_numbers),
                                  fargs=(test_series, pips), interval=30, repeat=False)
    # ani = animation.FuncAnimation(fig, update_plot, frames=get_pips(numbers=10),
    #                           interval=100, repeat=True, repeat_delay=2000)
    plt.show()
