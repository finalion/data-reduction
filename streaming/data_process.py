#-*- coding:utf-8 -*-
import numpy as np
import pandas as pd
import os

intel_lab_data_path = r'C:\\Users\\surface\\OneDrive\\Documents\\aalto\\fincodes\\data\\intel-lab-data.txt'
pickle_path = 'intel-lab.data'


def read_intel_lab_data():
    """
    read interl lab data
    referrence: http://db.csail.mit.edu/labdata/labdata.html
    """
    if not os.path.exists(pickle_path):
        df = pd.read_table(intel_lab_data_path, sep=' ', header=None, names=[
            'date', 'time', 'epoch', 'moteid', 'temperature', 'humidity', 'light', 'voltage'])
        # preprocess the data: order by date and time
        # df['date'] = pd.to_datetime(df.date)
        df.insert(0, 'datetime', pd.to_datetime(
            df['date']) + pd.to_timedelta(df['time']))
        del df['date']
        del df['time']
        df.sort_values('datetime', inplace=True)
        df.dropna(axis=0, how='any', inplace=True)
        df = df[:10000]
        df.to_pickle(pickle_path)
    else:
        df = pd.read_pickle(pickle_path)
    return df


def generate_data(data_frame, column_names):
    """
    column_names: string or strings list of the column names to be generated.
    """
    for i in range(len(data_frame)):
        row = data_frame.iloc[i]
        yield row[column_names]
