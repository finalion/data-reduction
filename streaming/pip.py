# coding: utf-8
import numpy as np


def distance(pc, pl, pr, dtype):
    # pc, pl, pr type is DataItem, so x coordinate is pc.value[0], y coordinate is pc.value[1]
    # make sure coordinates of the input points are float type
    d = 0
    x, y = pc.index, pc.value
    xl, yl = pl.index, pl.value
    xr, yr = pr.index, pr.value
    if dtype == "e":  # euclidean distance
        dl = np.sqrt((x - xl)**2 + (y - yl)**2)
        dr = np.sqrt((x - xr)**2 + (y - yr)**2)
        d = dl + dr
    elif dtype == "v":        # vertical distance
        if yl != yr:
            line_slope = (yr - yl) / (xr - xl)
            line_intercept = (xr * yl - yr * xl) / (xr - xl)
            cross_x = x
            cross_y = line_slope * cross_x + line_intercept
        else:
            cross_y = yr
        d = np.abs(y - cross_y)
    elif dtype == "p":    # perpendicular distance
        cl = np.sqrt((x - xl)**2 + (y - yl)**2)
        cr = np.sqrt((x - xr)**2 + (y - yr)**2)
        lr = np.sqrt((xl - xr)**2 + (yl - yr)**2)
        t = (cl + cr + lr) / 2
        d = 2 * np.sqrt(t * (t - cl) * (t - cr) * (t - lr)) / lr
    return d


def get_pip(input_series, index_first, index_last, dtype='e'):
    index = 0
    d = -1
    series = input_series[index_first:index_last + 1]
    for i, point in enumerate(series[1:-1]):
        # point: DataItem
        dt = distance(point, series[0], series[-1], dtype)
        # print index_first+1+i, point, dt
        index, d = (i + 1, dt) if dt > d else (index, d)
    return index + index_first


def get_pips(series, **kwargs):
    # parse the parameter. if 'numbers' specified,
    # then yield 'numbers' points, otherwise, output all the points by
    # importance order.
    numbers = kwargs.get('numbers', len(series))
    if numbers > len(series):
        numbers = len(series)
    # print numbers, '>>>>>>>>>>>>>>>>>>>>>>'
    input_series = series
    index_first, index_last = 0, len(input_series) - 1
    processing = [index_first, index_last]
    # pips = [index_first, index_last]
    yield index_first
    yield index_last
    i = 0
    while len(processing) != numbers:
        index_first, index_last = processing[i], processing[i + 1]
        if index_last == index_first + 1:  # two adjacent points
            i += 1
        # [i0, i2], in this serie, only have one point
        elif index_last == index_first + 2:
            index_pip = index_first + 1
            processing.insert(i + 1, index_pip)
            # pips.append(index_pip)
            yield index_pip
            i += 2
        else:
            index_pip = get_pip(input_series, index_first, index_last, 'v')
            processing.insert(i + 1, index_pip)
            # pips.append(index_pip)
            yield index_pip
            i += 2

        # reach the end of the processing list, restart to find unprocessed
        # data point
        if i == len(processing) - 1:
            i = 0
    # return pips
