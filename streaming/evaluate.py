#-*- coding:utf-8 -*-
from __future__ import division
import numpy as np
from streamify import DataItem
from scipy.interpolate import CubicSpline

# save the last sequence points to process overlapping
last_seq = []


class UnknownInterpolateException(Exception):
    pass


def get_interpolate_position(start_point, end_point):
    """
    get positions to be interpolated.
    """
    return range(start_point.gindex + 1, end_point.gindex)


def interpolate(start_point, end_point,  method='linear'):
    if method == 'linear':
        positions = get_interpolate_position(start_point, end_point)
        values = np.interp(positions, [start_point.gindex, end_point.gindex], [
            start_point.value, end_point.value])
        return [DataItem(positions[i], v) for i, v in enumerate(values)]
    elif method == 'cubic':
        # cs = CubicSpline(x, y)
        # positions = get_interpolate_position(start_point, end_point)
        # values = cs(positions)
        pass
    else:
        raise UnknownInterpolateException


def interpolate2(prev_point, current_point, post_point):
    x = prev_point.gindex, current_point.gindex, post_point.gindex
    y = prev_point.value, current_point.value, post_point.value
    cs = CubicSpline(x, y)
    positions = [
        get_interpolate_position(prev_point, current_point),
        get_interpolate_position(current_point, post_point)
    ]
    values_lst = [cs(positions[0]), cs(positions[1])]
    return [
        [DataItem(positions[0][i], v) for i, v in enumerate(values_lst[0])],
        [DataItem(positions[1][i], v) for i, v in enumerate(values_lst[1])]
    ]


def recover(forwarded_series, origin_series):
    recovered_series, accuracy = [], 0
    for i, each in enumerate(forwarded_series):
        if i == 0:
            assert each.gindex == 0
            recovered_series.append(each)
            continue
        prev = forwarded_series[i - 1]
        # if each.gindex != prev.gindex + 1:
        # for linear interpolation
        # points = interpolate(forwarded_series[i - 1], each)
        # recovered_series += points
        if i < len(forwarded_series) - 1:
            points_seq = interpolate2(
                forwarded_series[i - 1], each, forwarded_series[i + 1])
            global last_seq
            if last_seq:
                assert len(last_seq) == len(points_seq[0])
                for j, each in enumerate(points_seq[0]):
                    each.value = (last_seq[j].value + each.value) / 2
            recovered_series += points_seq[0]
            last_seq = points_seq[1]
        else:
            recovered_series += last_seq
        recovered_series.append(each)

    # assert len(recovered_series) == len(origin_series) # may not equal.
    # if len(recovered_series) < len(origin_series):
        # the last point of origin_series is not forwarded_series
    # recovered_series.extend(
    #     [0] * (len(origin_series) - len(recovered_series)))
    jaccard = [0, 0]
    for i, o in enumerate(recovered_series):
        jaccard[0] += min(o.value, origin_series[i].value)
        jaccard[1] += max(o.value, origin_series[i].value)
    accuracy = jaccard[0] / jaccard[1]
    return recovered_series, accuracy
