# coding: utf-8
#-*- coding:utf-8 -*-
from __future__ import division
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation
import time
from itertools import count
from pip import get_pips
from data_process import read_intel_lab_data, generate_data
# projection strategy definition
CLONE, TWIN, AVG = 0, 1, 2
CACHE_MAX_SIZE = 100
pip_measure = 'e'  # e: euclidean, v: vertical, p: perpendicular
IMPORTANCE_THRESHOLD = 0.3


class DataItem(object):
    _count = count(0)

    def __init__(self, gindex, value, ts_name='', timestamp=None, is_projection=False):
        self.name = ts_name
        # index in current cache
        self.index = 0
        # index in the entire series
        # self.gindex = -1 if is_projection else self._count.next()
        self.gindex = gindex
        self.value = value
        self.timestamp = timestamp if timestamp else int(1000 * time.time())
        self.is_projection = is_projection
        self.importance = 0
        self.order = -1
        # to evaluate
        self.values_overlapping = []

    # @property
    # def value(self):
    #     if len(self.values_overlapping) == 1:
    #         return self.values_overlapping[0]
    #     elif len(self.values_overlapping) == 0:
    #         return 0
    #     else:
    #         return np.average(self.values_overlapping)

    @property
    def coordinate(self):
        return self.index, self.value

    def projection(self):
        return DataItem(self.gindex, self.value, self.name, self.timestamp, True)

    def __str__(self):
        return "(%d, %f, %d)" % (self.index, self.value, self.is_projection)


class Cache(list):

    def __init__(self, *args):
        list.__init__(self, *args)

    def average(self):
        value_list = [item.value for item in self]
        return np.average(value_list)

    def projection(self):
        return Cache([item.projection() for item in self])

    def reindex(self):
        for i, item in enumerate(self):
            item.index = i

    def find_index(item):
        for i, each in enumerate(self):
            if each.timestamp == item.timpstamp:
                return i

    def __str__(self):
        return str([(item.index, item.value, item.is_projection) for item in self])

cache = Cache()
# cache_projection = Cache()


def handle(number, incoming_data_value, timestamp=None, projection_strategy=TWIN):
    """handle the incoming data item and calc the importance."""
    if not timestamp:
        timestamp = int(1000 * time.time())
    item = DataItem(number, incoming_data_value, timestamp=timestamp)
    # store the incoming data to the cache
    global cache
    cache.append(item)
    # store the incoming data to the cache projection by different strategies.
    cache_projection = Cache()
    cache_projection.extend(cache)
    if projection_strategy == CLONE:
        cache_projection.append(item.projection())
    elif projection_strategy == TWIN:
        cache_projection.extend(cache.projection())
    elif projection_strategy == AVG:
        item_copy = DataItem(-1, cache.average(),
                             timestamp=timestamp, is_projection=True)
        cache_projection.append(item_copy)
    else:
        print "wrong projection stragegy parameter, changed to default CLONE."
        cache_projection.append(item.projection())
    # re-index the cache projection_strategy
    cache_projection.reindex()
    # print 'cache_projection: ', cache_projection
    # get the order list by PIP algorithm
    entire_order = list(get_pips(cache_projection))
    order = [pip for pip in entire_order if not cache_projection[pip].is_projection]

    # find the current received data
    position = -1
    for p, o in enumerate(order):
        _item = cache_projection[o]
        if not _item.is_projection and _item.timestamp == timestamp:
            position = p
            break
    # calculate the importance degree
    importance = 1 - position / len(order)
    item.importance, item.order = importance, position
    # cache reduction
    if len(cache) == CACHE_MAX_SIZE:
        cache.pop(0)
    return item


def test_random_data(length, distance_strategy, importance_threshold):
    series = np.random.rand(length) * 10
    for data in series:
        importance, order = handle(data)
        if importance >= importance_threshold:
            print 'received data: %f, importance: %f, order: %d, forwarded --> ' % (data, importance, order)
        else:
            print 'received data: %f, importance: %f, order: %d, skipped.' % (data, importance, order)

orig_series = []
pip_series = []


def receive_data(generator, count):
    global orig_series, pip_series
    for i, data in enumerate(generator):
        if i >= count:
            break
        if np.isnan(data):
            continue
        item = handle(i, data)
        orig_series.append(item)
        if item.importance >= IMPORTANCE_THRESHOLD:
            print 'received %d data: %f, importance: %f, order: %d, forwarded --> ' % (i, data, item.importance, item.order)
            pip_series.append(item)
        else:
            print 'received %d data: %f, importance: %f, order: %d, skipped.' % (i, data, item.importance, item.order)
        yield item


if __name__ == '__main__':
    #-------------------------------------------------receive_data

    # test random data
    # test_random_data(1000, pip_measure, importance_threshold)
    # test intel lab data
    data_frame = read_intel_lab_data()
    generator = generate_data(data_frame, 'temperature')

    update_plot.data = list()
    fig = plt.figure()
    axes = plt.axes(xlim=(-1, CACHE_MAX_SIZE + 1), ylim=(-1, 40))
    # lines, = axes.plot(test_x, test_y, marker='.', color='green')
    collection = axes.scatter(
        [], [], marker='o', color='green', s=120, alpha=.6)

    ani = animation.FuncAnimation(fig, update_plot, fargs=(
        axes,), frames=receive_data(generator, 200), interval=20, repeat=False)

    plt.show()

    recover(pip_series, orig_series)
    # for data in generator:
    #     if np.isnan(data):
    #         continue
    #     item = handle(data)
    #     if item.importance >= importance_threshold:
    #         print 'received data: %f, importance: %f, order: %d, forwarded --> ' % (data, item.importance, item.order)
    #     else:
    # print 'received data: %f, importance: %f, order: %d, skipped.' % (data,
    # item.importance, item.order)
