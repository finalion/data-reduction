#-*- coding:utf-8 -*-
import matplotlib.pyplot as plt
from matplotlib import animation
from data_process import read_intel_lab_data, generate_data
from streamify import receive_data, CACHE_MAX_SIZE, IMPORTANCE_THRESHOLD, cache, pip_series, orig_series
from evaluate import recover
# importance_threshold = 0.5


def update_plot(data_item, axes):
    # update_plot.data.append(data_item.coordinate)
    update_plot.data = [each.coordinate for each in cache]
    collection.set_offsets(update_plot.data)
    collection.set_color(['red' if each.importance >
                          IMPORTANCE_THRESHOLD else 'green' for each in cache])
    # if data_item.importance > importance_threshold:
    #     axes.scatter([data_item.index,], [data_item.value,], marker='o', color='red', s=30,alpha=.6)
    return collection


data_frame = read_intel_lab_data()
generator = generate_data(data_frame, 'temperature')

update_plot.data = list()
fig = plt.figure()
axes = plt.axes(xlim=(-1, CACHE_MAX_SIZE + 1), ylim=(-1, 40))
# lines, = axes.plot(test_x, test_y, marker='.', color='green')
collection = axes.scatter(
    [], [], marker='o', color='green', s=120, alpha=.6)

ani = animation.FuncAnimation(fig, update_plot, fargs=(
    axes,), frames=receive_data(generator, 200), interval=5, repeat=False)

plt.show()

# recover the original data
recovered_series, accuracy = recover(pip_series, orig_series)
# plot the recovered data and actual original data for comparison
recovered_locations = [(each.gindex, each.value + 5)
                       for each in recovered_series]
rxs, rys = zip(*recovered_locations)
# plt.plot(rxs, rys, 'r--*')
rec = plt.scatter(rxs, rys, marker='o', color='red', s=120, alpha=.6)
origin_locations = [(each.gindex, each.value)
                    for each in orig_series]
oxs, oys = zip(*origin_locations)
# plt.plot(oxs, oys, 'g--o')
ori = plt.scatter(oxs, oys, marker='o', color='green', s=120, alpha=.6)
plt.legend((rec, ori), ('Recovered Series',
                        'Original Series'), scatterpoints=3)
plt.annotate('Accuracy: {}'.format(accuracy), xy=(
    orig_series[-1].gindex, orig_series[-1].value), xytext=(
    orig_series[-1].gindex, orig_series[-1].value))
plt.show()
